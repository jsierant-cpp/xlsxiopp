#pragma once

#include "sheet.hpp"

#include <xlsxio_read.h>

#include <cstdint>
#include <optional>
#include <string_view>

namespace xlsxio {

class Reader
{
public:
  static std::optional<Reader>
  load_data(std::string const& data);

  Reader(Reader const&) = delete;
  Reader(Reader&& r);
  Reader&
  operator=(Reader&&) = delete;
  Reader&
  operator=(Reader const&) = delete;
  ~Reader();

  std::optional<Sheet>
  get_sheet(std::string_view name, unsigned flags = XLSXIOREAD_SKIP_ALL_EMPTY);

private:
  explicit Reader(xlsxioreader h);

  xlsxioreader handle;
};

}

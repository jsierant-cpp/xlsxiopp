#pragma once

#include <xlsxio_read.h>

#include <cstdint>
#include <optional>
#include <stdexcept>
#include <string>

namespace xlsxio {

class Sheet
{
public:
  explicit Sheet(xlsxioreadersheet h);
  Sheet(Sheet&& s);
  Sheet(Sheet const&) = delete;
  Sheet&
  operator=(Sheet const&) = delete;
  Sheet&
  operator=(Sheet&&) = delete;
  ~Sheet();

  template<class... T>
  bool
  read_next_row(T&... value)
  {
    if (::xlsxioread_sheet_next_row(handle) == 0) {
      return false;
    }
    (read_cell(value), ...);
  }

  void
  skip_row(std::size_t nrows = 1u);

private:
  bool
  read_next_cell(double& value);
  bool
  read_next_cell(std::int64_t& value);
  bool
  read_next_cell(std::string& result);

  template<class T>
  void
  read_cell(std::optional<T>& opt_value)
  {
    T value;
    opt_value =
      read_next_cell(value) ? std::make_optional(value) : std::nullopt;
  }

  template<class T>
  void
  read_cell(T& value)
  {
    if (!read_next_cell(value)) {
      throw std::logic_error{ "xlsxio: missing mandatory cell value" };
    }
  }

  ::xlsxioreadersheet handle;
};

}

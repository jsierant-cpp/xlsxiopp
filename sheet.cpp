#include "include/xlsxio/sheet.hpp"

namespace xlsxio {

Sheet::Sheet(xlsxioreadersheet h)
  : handle{ h }
{
  if (handle == nullptr) {
    throw std::logic_error{ "xlsxio: mandatory handle pointer is not set" };
  }
}

Sheet::Sheet(Sheet&& s)
  : handle{ s.handle }
{
  s.handle = nullptr;
}

Sheet::~Sheet()
{
  if (handle != nullptr) {
    ::xlsxioread_sheet_close(handle);
    handle = nullptr;
  }
}

void
Sheet::skip_row(std::size_t nrows)
{
  for (std::size_t i = 0u; i < nrows; ++i) {
    ::xlsxioread_sheet_next_row(handle);
  }
}

bool
Sheet::read_next_cell(double& value)
{
  return ::xlsxioread_sheet_next_cell_float(handle, &value) != 0;
}

bool
Sheet::read_next_cell(std::int64_t& value)
{
  return ::xlsxioread_sheet_next_cell_int(handle, &value) != 0;
}
bool
Sheet::read_next_cell(std::string& result)
{
  char* value = nullptr;
  if (!xlsxioread_sheet_next_cell_string(handle, &value)) {
    return false;
  }
  if (value == nullptr) {
    throw std::logic_error{
      "xlsxio: expected string value to me set, internal error"
    };
  }
  result.assign(value);
  ::free(value);
  return true;
}
}

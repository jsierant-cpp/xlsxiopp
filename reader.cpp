#include "include/xlsxio/reader.hpp"

#include <cstdint>
#include <optional>
#include <string>

namespace xlsxio {

std::optional<Reader>
Reader::load_data(std::string const& data)
{
  auto* handle = ::xlsxioread_open_memory(
    const_cast<char*>(data.data()), data.size(), false);
  if (handle == nullptr) {
    return {};
  }
  return std::make_optional<Reader>(Reader{ handle });
}

Reader::Reader(Reader&& r)
  : handle{ r.handle }
{
  r.handle = nullptr;
}

Reader::~Reader()
{
  if (handle != nullptr) {
    ::xlsxioread_close(handle);
    handle = nullptr;
  }
}

std::optional<Sheet>
Reader::get_sheet(std::string_view name, unsigned flags)
{
  auto* sheet_handle = ::xlsxioread_sheet_open(handle, name.data(), flags);
  if (sheet_handle == nullptr) {
    return {};
  }
  return Sheet{ sheet_handle };
}

Reader::Reader(xlsxioreader h)
  : handle{ h }
{}

}
